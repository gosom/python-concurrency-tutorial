#-*- coding: utf-8 -*-
import socket
from expensive_functions import fibonacci


def simple_server(address):
    """Simple tcp socket server"""
    # create a tcp socket
    _sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    _sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    _sock.bind(address)
    _sock.listen(7)
    while True:
        client, addr = _sock.accept()
        print 'Connection {}'.format(addr)
        handler(client)


def handler(client):
    """Handles the incoming request"""
    _buff = 100
    while True:
        _bytes = client.recv(_buff)
        if _bytes:
            response = str(fibonacci(int(_bytes))).encode('ascii') + '\n'
            client.send(response)
        else:
            break


if __name__ == '__main__':
    simple_server(('', 5000))
