#-*- coding: utf-8 -*-

def fibonacci(n):
    """Very slow implementation of the fibonacci numbers"""
    return 1 if n <=2 else fibonacci(n-1) + fibonacci(n-2)
